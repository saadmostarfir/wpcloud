<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpressDB' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'ufomp88Ms-N}^=Q< ];2{ 24+s{rftx[n7`>&4!Ygxf(z/dC7ZZc+m@hxk)FM?|.' );
define( 'SECURE_AUTH_KEY',  'k8#RlCReb%C-zsL9jK ,wp*dgvhtJvO|I[<]h+2e-FUOC%P.+wm??@pPcDHpa]|>' );
define( 'LOGGED_IN_KEY',    'vN^zynH.0r9&AvCRRi O6|w$}gz`8aQ43{++tNJa`)jdO2l!f()`!*EB`u8lH9vW' );
define( 'NONCE_KEY',        's32-G6^A%DEGy~mo,7/E^{d?HnR)Yt55W1@u37TD4^QOigJrI.V9ZL&@95vlH^-S' );
define( 'AUTH_SALT',        'reDUh>,%^*{Jmh[Cu-]iu2qBrj}kClAqR0w:kwiP5M!2f#Vo/s/E&QjsBJJFE677' );
define( 'SECURE_AUTH_SALT', '5S_*yTEX:S7F}f[l`IQCib_,=]vSGOD8QHNexi!(d(v(ah{Xv!yYaTydeWa9Kcpw' );
define( 'LOGGED_IN_SALT',   'aX%^)PbFJm3:rhna*]?^:a]%GqNw8(~CAPo.s1Y*G(R|;#m?;YCOFM;=~(C<u2WW' );
define( 'NONCE_SALT',       'LWyvn0ECx.wMdwlrF2I6.QgL qj!N};Pfl$HNmf!/iZ[aCn,`YWBWNtJ^xqufI53' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
